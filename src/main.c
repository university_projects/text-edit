#include <stdlib.h>
#include <curses.h>
#include <string.h>

#include "MenuOption.h"
#include "MenuFunctions.h"
#include "Editor.h"
#include "ManagementMenu.h"

#ifdef  KEY_ENTER
#undef  KEY_ENTER
#define KEY_ENTER 10
#endif

int max_x, max_y;

int main ()
{
	enum MainMenuOptions {Management, Edit, Quit} option = Management;
	
	MenuOption managementOption, editOption, quitOption;
	initOption (&managementOption, 4, 0, "   Gestion          ");
	initOption (&editOption, 5, 0, "   Edicion          ");
	initOption (&quitOption, 6, 0, "   Salir            ");
	
	initscr ();
	raw ();
	noecho ();
	keypad (stdscr, true);
	getmaxyx (stdscr, max_y, max_x);
	--max_x;
	
	if (! has_colors ())
	{
		endwin ();
		printf ("Tu terminal no soporta colores\n");
		return 1;
	}
	
	start_color ();
	init_pair (1, COLOR_CYAN, COLOR_BLACK);
	init_pair (2, COLOR_RED, COLOR_WHITE);
	init_pair (3, COLOR_WHITE, COLOR_BLACK);
	init_pair (4, COLOR_GREEN, COLOR_BLACK);
	
	paintMenu (&managementOption, &editOption, &quitOption);

	for (register int key;;)
	{
		key = getch ();
		switch (key)
		{
			case KEY_UP:
				switch (option)
				{
					case Management:
						option = Quit;
						highlithOption (&managementOption, 3, stdscr);
						highlithOption (&quitOption, 2, stdscr);
					break;
					
					case Edit:
						option = Management;
						highlithOption (&editOption, 3, stdscr);
						highlithOption (&managementOption, 2, stdscr);
					break;
					
					case Quit:
						option = Edit;
						highlithOption (&quitOption, 3, stdscr);
						highlithOption (&editOption, 2, stdscr);
				}
			break;
			
			case KEY_DOWN:
				switch (option)
				{
					case Management:
						option = Edit;
						highlithOption (&managementOption, 3, stdscr);
						highlithOption (&editOption, 2, stdscr);
					break;
					
					case Edit:
						option = Quit;
						highlithOption (&editOption, 3, stdscr);
						highlithOption (&quitOption, 2, stdscr);
					break;
					
					case Quit:
						option = Management;
						highlithOption (&quitOption, 3, stdscr);
						highlithOption (&managementOption, 2, stdscr);
				}
			break;
			
			case KEY_ENTER:
				switch (option)
				{
					case Management:
						managementMenu ();
						paintMenu (&managementOption, &editOption, &quitOption);
						option = Management;
					break;

					case Edit:
						editor ();
						paintMenu (&managementOption, &editOption, &quitOption);
						option = Management;
					break;					
					
					case Quit:
						endwin ();
						return 0;
				}
		}

		refresh ();
	}
}
