#include "MenuOption.h"
#include <string.h>

/////////////////////////////////////////////
void initOption (MenuOption *option, int y, int x, const char *text)
{
	option->x = x;
	option->y = y;
	strcpy (option->text, text);
}

/////////////////////////////////////////////
void printOption (MenuOption *option, WINDOW *window)
{
	mvprintw (option->y, option->x, option->text, window);
}

/////////////////////////////////////////////
void highlithOption (MenuOption *option, int color, WINDOW * window)
{
	attron (COLOR_PAIR (color));
	mvprintw (option->y, option->x, option->text, window);
	attroff (COLOR_PAIR (color));
}
