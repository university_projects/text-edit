#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>

#include "Editor.h"

extern int max_x, max_y;

/////////////////////////////////////////////
void editor ()
{
	int line = 1, column = 0, totalLines = 1;
	char fileName [50] = "Archivo nuevo :)";
	Line *lines = 0;
	addNewLine (&lines, 0, 1);
	Line *currentLine = getLine (&lines, 0);

	clear ();
	echo ();

	printFileName (fileName);
	printTitle ();
	printOptions ();

	for (register int key;;)
	{
		printStatus (line, column);
		key = getch ();
		
		switch (key)
		{
			case KEY_UP:
				if (line > 1)
				{
					move (--line, column);
					currentLine = getLine (&lines, line - 1);
				}
			break;

			case KEY_DOWN:
				if (line < totalLines)
				{
					move (++line, column);
					currentLine = getLine (&lines, line - 1);
				}
			break;
			
			case KEY_LEFT:
				if (column > 0)
					move (line, --column);
				else if (line > 1)
				{
					currentLine = getLine (&lines, line - 2);
					move (--line, column = currentLine->length);
				}
			break;
			
			case KEY_RIGHT:
				if (column < currentLine->length)
					move (line, ++column);
				else if (line < totalLines)
				{
					move (line++, column = 0);
					currentLine = getLine (&lines, line - 1);
				}
			break;

			case KEY_ENTER:
				move (++line, column = 0);
				insertln ();
				printOptions ();
				++totalLines;
				addNewLine (&lines, line - 1, line);
				currentLine = getLine (&lines, line - 1);
			break;
			
			case KEY_BACKSPACE:
				if (column > 0)
				{
					delch ();
					--column;
					--(currentLine)->length;
				}
				else if (totalLines > 1 && line > 1)
				{
					deleteln ();
					--totalLines;
					--line;
					deleteLine (&lines, line - 1);
					
					move (max_y - 2, 0);
					clrtoeol ();
					printOptions ();
				}	
			break;
			
			default:
				++column;
				++(currentLine)->length;
		}

		if (! strcmp (keyname (key), "^Q"))
		{
			clear ();
			noecho ();
			deleteAllLines (&lines);
			return;
		}
		else if (! strcmp (keyname (key), "^S"))
			saveFile (fileName, &lines);
		else if (! strcmp (keyname (key), "^G"))
			saveAs (fileName, &lines);
		else if (! strcmp (keyname (key), "^O"))
			openFile (fileName, &lines, &totalLines);
		else if (! strcmp (keyname (key), "^W"))
			closeFile (fileName, &lines, &totalLines, &line, &column);

		refresh ();
	}
}

/////////////////////////////////////////////
static void printTitle ()
{
	attron (A_BOLD);
	attron (COLOR_PAIR (1));
	
	mvprintw (0, (max_x - 1) / 2 - 10, "Text editor :)");
	mvprintw (0, (max_x - 1) / 2 + 10, "Ln 1,   Col 0   ");

	attroff (COLOR_PAIR (1));
	attroff (A_BOLD);
}

/////////////////////////////////////////////
static void printFileName (char *fileName)
{
	attron (A_BOLD);
	attron (COLOR_PAIR (3));
	
	mvprintw (0, 5, fileName);
	
	attroff (A_BOLD);
	attroff (COLOR_PAIR (3));
}

/////////////////////////////////////////////
static void printOptions ()
{
	attron (A_BOLD);
	attron (COLOR_PAIR (1));

	mvprintw (max_y - 1, 10, "Ctrl + Q [Salir]    Ctrl + O [Abrir archivo]    Ctrl + S [Guardar archivo]    Ctrl + G [Guardar como]    "
							 "Ctrl + W [Cerrar archivo]"); //

	attroff (COLOR_PAIR (1));
	attroff (A_BOLD);
}

/////////////////////////////////////////////
static void printStatus (int line, int column)
{
	mvprintw (0, (max_x - 1) / 2 + 10, "Ln %d, Col %d     ", line, column + 1);
	move (line, column);
}

/////////////////////////////////////////////
static void getFileName (char *fileName)
{
	move (max_y - 1, 0);
	clrtoeol ();
	
	attron (COLOR_PAIR (1));
	printw ("Introduce la direccion del archivo:   ");
	attroff (COLOR_PAIR (1));

	scanw ("%s", fileName);
	
	move (max_y - 1, 0);
	clrtoeol ();
	printOptions ();
}

/////////////////////////////////////////////
void openFile (char *fileName, Line **lines, int *totalLines)
{
	getFileName (fileName);

	FILE *file = fopen (fileName, "r");
	char currentChar;
	int i = 0;
	Line *ln = *lines;
	move (1, 0);

	for (;;)
	{
		currentChar = (char) fgetc (file);
		
		switch (currentChar)
		{
			case '\n':
				addNewLine (lines, ++i, i + 1);
				ln = getLine (lines, i);
				++(*totalLines);
				move (i + 1, 0);
			break;
			
			case EOF:
				fclose (file);
				printFileName (fileName);
				printOptions ();
				move (1, 0);
				--(*totalLines);
				deleteLine (lines, i);
				return;
			
			default:
				printw ("%c", currentChar);
				++(ln)->length;
		}
	}	
}

/////////////////////////////////////////////
void saveFile (char *fileName, Line **lines)
{
	if (! strcmp (fileName, "Archivo nuevo :)"))
		getFileName (fileName);

	FILE *file = fopen (fileName, "w");
	int sizeDocument = getTotalLines (lines);

	for (register int i = 1; i <= sizeDocument; ++i)
	{
		Line *ln = getLine (lines, i - 1);
		for (register int j = 0; j <= ln->length; ++j)
			fprintf (file, "%c", (char) mvinch (i, j));
		fprintf (file, "\n");
	}

	fclose (file);
	printFileName (fileName);
}

/////////////////////////////////////////////
void saveAs (char *fileName, Line **lines)
{
	getFileName (fileName);

	FILE *file = fopen (fileName, "w");
	int sizeDocument = getTotalLines (lines);

	for (register int i = 1; i <= sizeDocument; ++i)
	{
		Line *ln = getLine (lines, i - 1);
		for (register int j = 0; j <= ln->length; ++j)
			fprintf (file, "%c", (char) mvinch (i, j));
		fprintf (file, "\n");
	}

	fclose (file);
	printFileName (fileName);
}

/////////////////////////////////////////////
void closeFile (char *fileName, Line **lines, int *totalLines, int *line, int *column)
{
	clear ();
	deleteAllLines (lines);
	
	fileName = "Archivo nuevo :)";
	*line = *totalLines = 1;
	*column = 0;
	addNewLine (lines, 0, 1);

	printTitle ();
	printFileName (fileName);
	printOptions ();
	move (1, 0);
}
