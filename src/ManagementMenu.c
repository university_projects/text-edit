#include <curses.h>
#include <string.h>
#include <stdlib.h>
#include "ManagementMenu.h"
#include "MenuFunctions.h"

extern int max_x, max_y;

/////////////////////////////////////////////
void managementMenu ()
{
	char nameFile [20], newNameFile [20];
	char fileLocation [50], newFileLocation [50];
	
	clear ();
	
	enum ManagementMenuOptions {DeleteFile, MoveFile, CopyFile, Back, RenameFile} menuOption = MoveFile;
	
	MenuOption moveFileOption, deleteFileOption, copyFileOption, backOption, renameFileOption;
	initOption (&moveFileOption, 4, 0, "   Mover          ");
	initOption (&copyFileOption, 5, 0, "   Copiar          ");
	initOption (&deleteFileOption, 6, 0, "   Borrar          ");
	initOption (&renameFileOption, 7, 0, "   Renombrar          ");
	initOption (&backOption, 8, 0, "   Regresar          ");

	paintManagementMenu (&deleteFileOption, &renameFileOption, &backOption, &moveFileOption, &copyFileOption);

	for (register int key;;)
	{
		key = getch ();
		switch (key)
		{
			case KEY_UP:
				switch (menuOption)
				{
					case MoveFile:
						menuOption = Back;
						highlithOption (&moveFileOption, 3, stdscr);
						highlithOption (&backOption, 2, stdscr);
					break;
					
					case CopyFile:
						menuOption = MoveFile;
						highlithOption (&copyFileOption, 3, stdscr);
						highlithOption (&moveFileOption, 2, stdscr);
					break;
					
					case DeleteFile:
						menuOption = CopyFile;
						highlithOption (&deleteFileOption, 3, stdscr);
						highlithOption (&copyFileOption, 2, stdscr);
					break;
					
					case RenameFile:
						menuOption = DeleteFile;
						highlithOption (&renameFileOption, 3, stdscr);
						highlithOption (&deleteFileOption, 2, stdscr);
					break;
					
					case Back:
						menuOption = RenameFile;
						highlithOption (&backOption, 3, stdscr);
						highlithOption (&renameFileOption, 2, stdscr);
				}
			break;
			
			case KEY_DOWN:
				switch (menuOption)
				{
					case MoveFile:
						menuOption = CopyFile;
						highlithOption (&moveFileOption, 3, stdscr);
						highlithOption (&copyFileOption, 2, stdscr);
					break;
					
					case CopyFile:
						menuOption = DeleteFile;
						highlithOption (&copyFileOption, 3, stdscr);
						highlithOption (&deleteFileOption, 2, stdscr);
					break;
					
					case DeleteFile:
						menuOption = RenameFile;
						highlithOption (&deleteFileOption, 3, stdscr);
						highlithOption (&renameFileOption, 2, stdscr);
					break;
					
					case RenameFile:
						menuOption = Back;
						highlithOption (&renameFileOption, 3, stdscr);
						highlithOption (&backOption, 2, stdscr);
					break;
					
					case Back:
						menuOption = MoveFile;
						highlithOption (&backOption, 3, stdscr);
						highlithOption (&moveFileOption, 2, stdscr);
				}
			break;
			
			case KEY_ENTER:
				clear ();
				switch (menuOption)
				{
					case MoveFile:
						echo ();
						paintTitle ("Mover Archivo");
						mvprintw (3, 0, "Indroduce la direccion del archivo:        ");
						scanw ("%s", fileLocation);
						mvprintw (4, 0, "Indroduce el nombre del archivo:           ");
						scanw ("%s", nameFile);
						mvprintw (5, 0, "Indroduce la nueva direccion del archivo:  ");
						scanw ("%s", newFileLocation);
						noecho ();
						
						char mv [50] = "mv ";
						strcat (mv, fileLocation);
						strcat (mv, nameFile);
						strcat (mv, " ");
						strcat (mv, newFileLocation);
						system (mv);
						
						printw ("Se ejecutó %s \nPresiona una tecla para continuar", mv);
						getch ();
						clear ();

						menuOption = MoveFile;
						paintManagementMenu (&deleteFileOption, &renameFileOption, &backOption, &moveFileOption, &copyFileOption);
					break;
					
					case CopyFile:
						echo ();
						paintTitle ("Copiar Archivo");
						mvprintw (3, 0, "Indroduce la direccion del archivo:          ");
						scanw ("%s",fileLocation);
						mvprintw (4, 0, "Indroduce el nombre del archivo:           ");
						scanw ("%s",nameFile);
						mvprintw (5, 0, "Indroduce la direccion del nuevo archivo:  ");
						scanw ("%s",newFileLocation);
						noecho ();
						
						char cp [50] = "cp -r ";
						strcat (cp, fileLocation);
						strcat (cp, nameFile);
						strcat (cp, " ");
						strcat (cp, newFileLocation);
						system (cp);
						
						printw ("Se ejecutó %s \nPresiona una tecla para continuar", cp);
						getch ();
						clear ();

						menuOption = MoveFile;
						paintManagementMenu (&deleteFileOption, &renameFileOption, &backOption, &moveFileOption, &copyFileOption);
					break;
					
					case DeleteFile:
						echo ();
						paintTitle ("Borrar Archivo");
						mvprintw (3, 0, "Indroduce la direccion del archivo:  ");
						mvscanw (3, 40, "%s",fileLocation);
						mvprintw (4, 0, "Indroduce el nombre del archivo:     ");
						mvscanw (4, 40, "%s",nameFile);
						
						char rm [60] = "rm -r ";

						strcat (rm, fileLocation);
						strcat (rm, nameFile);
						system (rm);
						
						printw ("Se ejecutó %s \nPresiona una tecla para continuar", rm);
						getch ();
						clear ();

						menuOption = MoveFile;
						paintManagementMenu (&deleteFileOption, &renameFileOption, &backOption, &moveFileOption, &copyFileOption);
					break;
					
					case RenameFile:
						echo ();
						paintTitle ("Renombrar Archivo");
						mvprintw (3, 0, "Indroduce la direccion del archivo:     ");
						mvscanw (3, 40, "%s",fileLocation);
						mvprintw (4, 0, "Indroduce el nombre del archivo:        ");
						mvscanw (4, 40, "%s",nameFile);
						mvprintw (5, 0, "Indroduce el nuevo nombre del archivo:  ");
						mvscanw (5, 40, "%s",newNameFile);
						noecho ();

						char rename [60] = "mv ";
						strcat (rename, fileLocation);
						strcat (rename, nameFile);
						strcat (rename, " ");
						strcat (rename, newNameFile);

						system (rename);
						
						printw ("Se ejecutó %s \nPresiona una tecla para continuar", rename);
						getch ();
						clear ();

						menuOption = MoveFile;
						paintManagementMenu (&deleteFileOption, &renameFileOption, &backOption, &moveFileOption, &copyFileOption);
					break;
					
					case Back:
						return;
				}
		}
	}
}

void paintTitle (char *title)
{
	attron (COLOR_PAIR (4));
	attron (A_BOLD);
	mvprintw (1, max_x/ 2 - 8, title);
	attroff (A_BOLD);
	attroff (COLOR_PAIR (1));
}
