#include "Line.h"
#include <stdlib.h>

/////////////////////////////////////////////
int addNewLine (Line **head, int position, int lineNumber)
{
	if ( position < 0 || position > getTotalLines (head))
		return 0;

	Line *newLine = malloc (sizeof (Line));
	newLine->number = lineNumber;
	newLine->length = 0;
	
	if (! *head || ! position) // In the beginning
	{
		newLine->next = *head;
		*head = newLine;
		return 1;
	}

	Line *nextLine = *head, *previousLine;

	for (int i = 0; i < position; ++i) // another position
	{
		previousLine = nextLine;
		nextLine = nextLine->next;
	}
	
	newLine->next = nextLine;
	previousLine->next = newLine;
	
	return 1;
}

/////////////////////////////////////////////
void deleteLine (Line **head, int position)
{
	if (! *head || position < 0 || ! getTotalLines (head) )
		return;

	Line *lineToBeDeleted = *head;

	if (! position) // the head
	{
		*head = (*head)->next;
		free (lineToBeDeleted);
		return;
	}
	
	Line *previousLine;
	
	for (int i = 0; i < position; ++i) // another position
	{
		previousLine = lineToBeDeleted;
		lineToBeDeleted = lineToBeDeleted->next;
	}
	
	previousLine->next = lineToBeDeleted->next;
	free (lineToBeDeleted);
}

/////////////////////////////////////////////
void deleteAllLines (Line **head)
{
	while (getTotalLines (head))
		deleteLine (head, 0);
}

/////////////////////////////////////////////
int getTotalLines (Line **start)
{
	Line *aux = *start;
	int i = 0;
	
	while (aux)
	{
		++i;
		aux = aux->next;
	}
	
	return i;
}

/////////////////////////////////////////////
Line * getLine (Line **head, int position)
{
	if (! *head || position < 0 || ! getTotalLines (head))
		return 0;

	Line *lineToBeReturned = *head;
	
	for (int i = 0; i < position; ++i)
		lineToBeReturned = lineToBeReturned->next;

	return lineToBeReturned;
}

/////////////////////////////////////////////
int getLineNumber (Line **head, int position)
{
	if (! *head || position < 0 || ! getTotalLines (head))
		return 0;

	Line *currentLine = *head;
	
	for (int i = 0; i < position; ++i)
		currentLine = currentLine->next;

	return currentLine->number;
}

/////////////////////////////////////////////
void setLineNumber (Line **head, int position, int lineNumber)
{
	if (! *head || position < 0 || ! getTotalLines (head))
		return;

	Line *currentLine = *head;
	
	for (int i = 0; i < position; ++i)
		currentLine = currentLine->next;

	currentLine->number = lineNumber;
}

/////////////////////////////////////////////
int getLineLength (Line **head, int position)
{
	if (! *head || position < 0 || ! getTotalLines (head))
		return 0;

	Line *currentLine = *head;
	
	for (int i = 0; i < position; ++i)
		currentLine = currentLine->next;

	return currentLine->length;
}

/////////////////////////////////////////////
void setLineLenght (Line **head, int position, int addOrSubstract)
{
	if (! *head || position < 0 || ! getTotalLines (head))
		return;

	Line *currentLine = *head;
	
	for (int i = 0; i < position; ++i)
		currentLine = currentLine->next;

	(addOrSubstract >= 0) ? ++(currentLine)->length : --(currentLine)->length;
}
