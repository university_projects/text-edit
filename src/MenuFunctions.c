#include "MenuFunctions.h"

extern int max_x, max_y; // defined in main.c

/////////////////////////////////////////////
void paintMenu (MenuOption *managementOption, MenuOption *editOption, MenuOption *quitOption)
{
	attron (COLOR_PAIR (1));
	attron (A_BOLD);
	mvprintw (1, max_x / 2 - 8, "Menu principal");
	attroff (A_BOLD);
	attroff (COLOR_PAIR (1));
	
	printOption (managementOption, stdscr);
	printOption (editOption, stdscr);
	printOption (quitOption, stdscr);
	
	highlithOption (managementOption, 2, stdscr);	
}

/////////////////////////////////////////////
void paintManagementMenu (MenuOption *deleteFileOption, MenuOption *renameFileOption, MenuOption *backOption, MenuOption *moveFileOption, 							  MenuOption *copyFileOption)
{
	attron (COLOR_PAIR (1));
	attron (A_BOLD);
	mvprintw (1, 60, "Gestor de Archivos");
	attroff (A_BOLD);
	attroff (COLOR_PAIR (1));
	
	printOption (deleteFileOption, stdscr);
	printOption (moveFileOption, stdscr);
	printOption (copyFileOption, stdscr);
	printOption (renameFileOption, stdscr);
	printOption (backOption, stdscr);
	
	highlithOption (moveFileOption, 2, stdscr);	
}

