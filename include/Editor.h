#ifndef TEXT_EDIT_EDITOR_H
#define TEXT_EDIT_EDITOR_H

#ifdef  KEY_ENTER
#undef  KEY_ENTER
#define KEY_ENTER 10
#endif

#include "Line.h"

void editor ();
static void printTitle ();
static void printFileName (char *fileName);
static void printOptions ();
static void printStatus (int line, int column);
static void getFileName (char *fileName);

void openFile (char *fileName, Line **lines, int *totalLines);
void saveFile (char *fileName, Line **lines);
void saveAs (char *fileName, Line **lines);
void closeFile (char *fileName, Line **lines, int *totalLines, int *line, int *column);

#endif // TEXT_EDIT_EDITOR_H
