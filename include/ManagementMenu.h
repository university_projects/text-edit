#ifndef TEXT_EDIT_MANAGEMENT_MENU_H
#define TEXT_EDIT_MANAGEMENT_MENU_H

#ifdef  KEY_ENTER
#undef  KEY_ENTER
#define KEY_ENTER 10
#endif

void managementMenu ();
void paintTitle (char *title);

#endif // TEXT_EDIT_MANAGEMENT_MENU_H
