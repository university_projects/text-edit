#ifndef TEXT_EDIT_LINE_H
#define TEXT_EDIT_LINE_H

struct Line
{
	int length;
	int number;
	struct Line *next;
};

typedef struct Line Line;

int addNewLine (Line **head, int position, int lineNumber);
void deleteLine (Line **head, int position);
void deleteAllLines (Line **head);
int getTotalLines (Line **start);
Line * getLine (Line **head, int position);
int getLineNumber (Line **head, int position);
void setLineNumber (Line **head, int position, int lineNumber);
int getLineLength (Line **head, int position);
void setLineLenght (Line **head, int position, int addOrSubstract);

#endif // TEXT_EDIT_LINE_H
