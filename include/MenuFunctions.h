#ifndef TEXT_EDIT_MENU_FUNCTIONS_H
#define TEXT_EDIT_MENU_FUNCTIONS_H

#include "MenuOption.h"

void paintMenu (MenuOption *managementOption, MenuOption *editOption, MenuOption *quitOption);
void paintManagementMenu (MenuOption *deleteFileOption, MenuOption *renameFileOption, MenuOption *backOption, MenuOption *moveFileOption, 							  MenuOption *copyFileOption);

#endif // TEXT_EDIT_MENU_FUNCTIONS_H
