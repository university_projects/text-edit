/////////////////////////////////////////////
/// \file  MenuOption.h
/////////////////////////////////////////////

#ifndef TEXT_EDIT_MENU_OPTION_H
#define TEXT_EDIT_MENU_OPTION_H

#include <curses.h>

/////////////////////////////////////////////
/// \brief Struct for manage menu options :).
/////////////////////////////////////////////
typedef struct
{
	int y; 			///< Y Coordinate where it'll be drawn.
	int x; 			///< X Coordinate where it'll be drawn.
	char text [20]; ///< Text that it'll show.
} MenuOption;

/////////////////////////////////////////////
/// \brief Initializes a MenuOption type.
///
/// \param y  	 Y Coordinate where it'll be drawn.
/// \param x 	 X Coordinate where it'll be drawn.
/// \param text  Text that it'll show.
/////////////////////////////////////////////
void initOption (MenuOption *option, int y, int x, const char *text);

/////////////////////////////////////////////
/// \brief Print a MenuOption type in some window.
///
/// \param option Option to be printed.
/// \param window Window where the option will be printed.
/////////////////////////////////////////////
void printOption (MenuOption *option, WINDOW *window);

/////////////////////////////////////////////
/// \brief Higlight a MenuOption with a specific color.
///
/// The color is from COLOR_PAIR. It's called like
/// \code
/// 	COLOR_PAIR (color)
/// \endcode
///
/// \param option Option to be highlighted.
/// \param color  Color that will use to highligh the option.
/// \param window Window where the option will be highlighted.
/////////////////////////////////////////////
void highlithOption (MenuOption *option, int color, WINDOW *window);

#endif // TEXT_EDIT_MENU_OPTION_H
